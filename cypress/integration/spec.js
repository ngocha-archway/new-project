/// <reference types="Cypress" />

context('Actions', () => {
  beforeEach(() => {
    cy.visit('https://ngocha-archway.gitlab.io/new-project/')
  })

  it('Title', () => {

    cy.get('.v-toolbar__title').contains('Vuetify.js')
    cy.get('.v-btn__content').contains('Continue').should('be.visible')

  })
})
